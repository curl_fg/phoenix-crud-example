defmodule CrudAgentes.AccountsTest do
  use CrudAgentes.DataCase

  alias CrudAgentes.Accounts

  describe "agents" do
    alias CrudAgentes.Accounts.Agent

    @valid_attrs %{age: 42, email: "some email", expire_license: ~D[2010-04-17], license: "some license", m_surname: "some m_surname", name: "some name", p_surname: "some p_surname"}
    @update_attrs %{age: 43, email: "some updated email", expire_license: ~D[2011-05-18], license: "some updated license", m_surname: "some updated m_surname", name: "some updated name", p_surname: "some updated p_surname"}
    @invalid_attrs %{age: nil, email: nil, expire_license: nil, license: nil, m_surname: nil, name: nil, p_surname: nil}

    def agent_fixture(attrs \\ %{}) do
      {:ok, agent} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_agent()

      agent
    end

    test "Mi 1 prueba" do
      assert 1 == 1
    end

    test "Mi 2 prueba" do
      assert 1 == 1
    end

    test "Mi 3 prueba" do
      assert 1 == 1
    end

    test "Mi 4 prueba" do
      assert 1 == 1
    end

    test "list_agents/0 returns all agents" do
      agent = agent_fixture()
      assert Accounts.list_agents() == [agent]
    end

    test "get_agent!/1 returns the agent with given id" do
      agent = agent_fixture()
      assert Accounts.get_agent!(agent.id) == agent
    end

    test "create_agent/1 with valid data creates a agent" do
      assert {:ok, %Agent{} = agent} = Accounts.create_agent(@valid_attrs)
      assert agent.age == 42
      assert agent.email == "some email"
      assert agent.expire_license == ~D[2010-04-17]
      assert agent.license == "some license"
      assert agent.m_surname == "some m_surname"
      assert agent.name == "some name"
      assert agent.p_surname == "some p_surname"
    end

    test "create_agent/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_agent(@invalid_attrs)
    end

    test "update_agent/2 with valid data updates the agent" do
      agent = agent_fixture()
      assert {:ok, %Agent{} = agent} = Accounts.update_agent(agent, @update_attrs)
      assert agent.age == 43
      assert agent.email == "some updated email"
      assert agent.expire_license == ~D[2011-05-18]
      assert agent.license == "some updated license"
      assert agent.m_surname == "some updated m_surname"
      assert agent.name == "some updated name"
      assert agent.p_surname == "some updated p_surname"
    end

    test "update_agent/2 with invalid data returns error changeset" do
      agent = agent_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_agent(agent, @invalid_attrs)
      assert agent == Accounts.get_agent!(agent.id)
    end

    test "delete_agent/1 deletes the agent" do
      agent = agent_fixture()
      assert {:ok, %Agent{}} = Accounts.delete_agent(agent)
      assert_raise Ecto.NoResultsError, fn -> Accounts.get_agent!(agent.id) end
    end

    test "change_agent/1 returns a agent changeset" do
      agent = agent_fixture()
      assert %Ecto.Changeset{} = Accounts.change_agent(agent)
    end
  end
end
