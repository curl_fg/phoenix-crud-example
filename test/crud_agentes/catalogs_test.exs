defmodule CrudAgentes.CatalogsTest do
  use CrudAgentes.DataCase

  alias CrudAgentes.Catalogs

  describe "states" do
    alias CrudAgentes.Catalogs.State

    @valid_attrs %{desciption: "some desciption", id_estado: 42}
    @update_attrs %{desciption: "some updated desciption", id_estado: 43}
    @invalid_attrs %{desciption: nil, id_estado: nil}

    def state_fixture(attrs \\ %{}) do
      {:ok, state} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Catalogs.create_state()

      state
    end

    test "list_states/0 returns all states" do
      state = state_fixture()
      assert Catalogs.list_states() == [state]
    end

    test "get_state!/1 returns the state with given id" do
      state = state_fixture()
      assert Catalogs.get_state!(state.id) == state
    end

    test "create_state/1 with valid data creates a state" do
      assert {:ok, %State{} = state} = Catalogs.create_state(@valid_attrs)
      assert state.desciption == "some desciption"
      assert state.id_estado == 42
    end

    test "create_state/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalogs.create_state(@invalid_attrs)
    end

    test "update_state/2 with valid data updates the state" do
      state = state_fixture()
      assert {:ok, %State{} = state} = Catalogs.update_state(state, @update_attrs)
      assert state.desciption == "some updated desciption"
      assert state.id_estado == 43
    end

    test "update_state/2 with invalid data returns error changeset" do
      state = state_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalogs.update_state(state, @invalid_attrs)
      assert state == Catalogs.get_state!(state.id)
    end

    test "delete_state/1 deletes the state" do
      state = state_fixture()
      assert {:ok, %State{}} = Catalogs.delete_state(state)
      assert_raise Ecto.NoResultsError, fn -> Catalogs.get_state!(state.id) end
    end

    test "change_state/1 returns a state changeset" do
      state = state_fixture()
      assert %Ecto.Changeset{} = Catalogs.change_state(state)
    end
  end

  describe "municipalities" do
    alias CrudAgentes.Catalogs.Municipality

    @valid_attrs %{desciption: "some desciption"}
    @update_attrs %{desciption: "some updated desciption"}
    @invalid_attrs %{desciption: nil}

    def municipality_fixture(attrs \\ %{}) do
      {:ok, municipality} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Catalogs.create_municipality()

      municipality
    end

    test "list_municipalities/0 returns all municipalities" do
      municipality = municipality_fixture()
      assert Catalogs.list_municipalities() == [municipality]
    end

    test "get_municipality!/1 returns the municipality with given id" do
      municipality = municipality_fixture()
      assert Catalogs.get_municipality!(municipality.id) == municipality
    end

    test "create_municipality/1 with valid data creates a municipality" do
      assert {:ok, %Municipality{} = municipality} = Catalogs.create_municipality(@valid_attrs)
      assert municipality.desciption == "some desciption"
    end

    test "create_municipality/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalogs.create_municipality(@invalid_attrs)
    end

    test "update_municipality/2 with valid data updates the municipality" do
      municipality = municipality_fixture()
      assert {:ok, %Municipality{} = municipality} = Catalogs.update_municipality(municipality, @update_attrs)
      assert municipality.desciption == "some updated desciption"
    end

    test "update_municipality/2 with invalid data returns error changeset" do
      municipality = municipality_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalogs.update_municipality(municipality, @invalid_attrs)
      assert municipality == Catalogs.get_municipality!(municipality.id)
    end

    test "delete_municipality/1 deletes the municipality" do
      municipality = municipality_fixture()
      assert {:ok, %Municipality{}} = Catalogs.delete_municipality(municipality)
      assert_raise Ecto.NoResultsError, fn -> Catalogs.get_municipality!(municipality.id) end
    end

    test "change_municipality/1 returns a municipality changeset" do
      municipality = municipality_fixture()
      assert %Ecto.Changeset{} = Catalogs.change_municipality(municipality)
    end
  end

  describe "zip_codes" do
    alias CrudAgentes.Catalogs.ZipCodes

    @valid_attrs %{desciption: "some desciption"}
    @update_attrs %{desciption: "some updated desciption"}
    @invalid_attrs %{desciption: nil}

    def zip_codes_fixture(attrs \\ %{}) do
      {:ok, zip_codes} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Catalogs.create_zip_codes()

      zip_codes
    end

    test "list_zip_codes/0 returns all zip_codes" do
      zip_codes = zip_codes_fixture()
      assert Catalogs.list_zip_codes() == [zip_codes]
    end

    test "get_zip_codes!/1 returns the zip_codes with given id" do
      zip_codes = zip_codes_fixture()
      assert Catalogs.get_zip_codes!(zip_codes.id) == zip_codes
    end

    test "create_zip_codes/1 with valid data creates a zip_codes" do
      assert {:ok, %ZipCodes{} = zip_codes} = Catalogs.create_zip_codes(@valid_attrs)
      assert zip_codes.desciption == "some desciption"
    end

    test "create_zip_codes/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Catalogs.create_zip_codes(@invalid_attrs)
    end

    test "update_zip_codes/2 with valid data updates the zip_codes" do
      zip_codes = zip_codes_fixture()
      assert {:ok, %ZipCodes{} = zip_codes} = Catalogs.update_zip_codes(zip_codes, @update_attrs)
      assert zip_codes.desciption == "some updated desciption"
    end

    test "update_zip_codes/2 with invalid data returns error changeset" do
      zip_codes = zip_codes_fixture()
      assert {:error, %Ecto.Changeset{}} = Catalogs.update_zip_codes(zip_codes, @invalid_attrs)
      assert zip_codes == Catalogs.get_zip_codes!(zip_codes.id)
    end

    test "delete_zip_codes/1 deletes the zip_codes" do
      zip_codes = zip_codes_fixture()
      assert {:ok, %ZipCodes{}} = Catalogs.delete_zip_codes(zip_codes)
      assert_raise Ecto.NoResultsError, fn -> Catalogs.get_zip_codes!(zip_codes.id) end
    end

    test "change_zip_codes/1 returns a zip_codes changeset" do
      zip_codes = zip_codes_fixture()
      assert %Ecto.Changeset{} = Catalogs.change_zip_codes(zip_codes)
    end
  end
end
