# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :crud_agentes,
  ecto_repos: [CrudAgentes.Repo]

# Configures the endpoint
config :crud_agentes, CrudAgentesWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "UeOfljkMbGiciDOi5SWWJCkPR+kPcGlJE0rIy9tA4+V31JTdBBQ0pw7ohEtZlePc",
  render_errors: [view: CrudAgentesWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: CrudAgentes.PubSub,
  live_view: [signing_salt: "cFbHPXx+"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
