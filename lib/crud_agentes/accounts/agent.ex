defmodule CrudAgentes.Accounts.Agent do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder,
           only: [:name, :age, :email, :p_surname, :m_surname, :license, :expire_license, :id]}

  schema "agents" do
    field :age, :integer
    field :email, :string
    field :expire_license, :date
    field :license, :string
    field :m_surname, :string
    field :name, :string
    field :p_surname, :string

    timestamps()
  end

  @doc false
  def changeset(agent, attrs) do
    agent
    |> cast(attrs, [:name, :age, :email, :p_surname, :m_surname, :license, :expire_license])
    |> validate_required([:name, :age, :email, :p_surname, :m_surname, :license, :expire_license])
  end
end
