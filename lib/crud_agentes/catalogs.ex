defmodule CrudAgentes.Catalogs do
  @moduledoc """
  The Catalogs context.
  """

  import Ecto.Query, warn: false
  alias CrudAgentes.Repo

  alias CrudAgentes.Catalogs.State
  alias CrudAgentes.Catalogs.ZipCodes
  alias CrudAgentes.Catalogs.Municipality

  @doc """
  Returns the list of states.

  ## Examples

      iex> list_states()
      [%State{}, ...]

  """
  def list_states do
    Repo.all(State)
  end

  def get_municipality_state(cp) do
    (
      from cp in ZipCodes,
        join: mun in Municipality, on: cp.id_municipality == mun.id,
        join: sta in State, on: mun.id_state == sta.id,
        select: %{cp: cp.desciption, municipio: mun.desciption, estado: sta.desciption},
        where: like(cp.desciption, ^"%#{cp}%")
        # where: cp.desciption == ^cp
    )
    |> Repo.all()
  end

  def get_state_municipalities() do
    (
      from state in State,
        #join: municipality in Municipality, on: state.id == municipality.id_state,
        #select: %{id: state.id, municipio: municipality.desciption},
        #preload: [:municipalities],
        where: state.id == 1
    )
    |> Repo.one!()
    |> Repo.preload([:municipalities, [:zip_code]])
  end

  @doc """
  Gets a single state.

  Raises `Ecto.NoResultsError` if the State does not exist.

  ## Examples

      iex> get_state!(123)
      %State{}

      iex> get_state!(456)
      ** (Ecto.NoResultsError)

  """
  def get_state!(id), do: Repo.get!(State, id)

  @doc """
  Creates a state.

  ## Examples

      iex> create_state(%{field: value})
      {:ok, %State{}}

      iex> create_state(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_state(attrs \\ %{}) do
    %State{}
    |> State.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a state.

  ## Examples

      iex> update_state(state, %{field: new_value})
      {:ok, %State{}}

      iex> update_state(state, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_state(%State{} = state, attrs) do
    state
    |> State.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a state.

  ## Examples

      iex> delete_state(state)
      {:ok, %State{}}

      iex> delete_state(state)
      {:error, %Ecto.Changeset{}}

  """
  def delete_state(%State{} = state) do
    Repo.delete(state)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking state changes.

  ## Examples

      iex> change_state(state)
      %Ecto.Changeset{data: %State{}}

  """
  def change_state(%State{} = state, attrs \\ %{}) do
    State.changeset(state, attrs)
  end

  alias CrudAgentes.Catalogs.Municipality

  @doc """
  Returns the list of municipalities.

  ## Examples

      iex> list_municipalities()
      [%Municipality{}, ...]

  """
  def list_municipalities do
    Repo.all(Municipality)
  end

  @doc """
  Gets a single municipality.

  Raises `Ecto.NoResultsError` if the Municipality does not exist.

  ## Examples

      iex> get_municipality!(123)
      %Municipality{}

      iex> get_municipality!(456)
      ** (Ecto.NoResultsError)

  """
  def get_municipality!(id), do: Repo.get!(Municipality, id)

  @doc """
  Creates a municipality.

  ## Examples

      iex> create_municipality(%{field: value})
      {:ok, %Municipality{}}

      iex> create_municipality(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_municipality(attrs \\ %{}) do
    %Municipality{}
    |> Municipality.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a municipality.

  ## Examples

      iex> update_municipality(municipality, %{field: new_value})
      {:ok, %Municipality{}}

      iex> update_municipality(municipality, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_municipality(%Municipality{} = municipality, attrs) do
    municipality
    |> Municipality.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a municipality.

  ## Examples

      iex> delete_municipality(municipality)
      {:ok, %Municipality{}}

      iex> delete_municipality(municipality)
      {:error, %Ecto.Changeset{}}

  """
  def delete_municipality(%Municipality{} = municipality) do
    Repo.delete(municipality)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking municipality changes.

  ## Examples

      iex> change_municipality(municipality)
      %Ecto.Changeset{data: %Municipality{}}

  """
  def change_municipality(%Municipality{} = municipality, attrs \\ %{}) do
    Municipality.changeset(municipality, attrs)
  end

  alias CrudAgentes.Catalogs.ZipCodes

  @doc """
  Returns the list of zip_codes.

  ## Examples

      iex> list_zip_codes()
      [%ZipCodes{}, ...]

  """
  def list_zip_codes do
    Repo.all(ZipCodes)
  end

  @doc """
  Gets a single zip_codes.

  Raises `Ecto.NoResultsError` if the Zip codes does not exist.

  ## Examples

      iex> get_zip_codes!(123)
      %ZipCodes{}

      iex> get_zip_codes!(456)
      ** (Ecto.NoResultsError)

  """
  def get_zip_codes!(id), do: Repo.get!(ZipCodes, id)

  @doc """
  Creates a zip_codes.

  ## Examples

      iex> create_zip_codes(%{field: value})
      {:ok, %ZipCodes{}}

      iex> create_zip_codes(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_zip_codes(attrs \\ %{}) do
    %ZipCodes{}
    |> ZipCodes.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a zip_codes.

  ## Examples

      iex> update_zip_codes(zip_codes, %{field: new_value})
      {:ok, %ZipCodes{}}

      iex> update_zip_codes(zip_codes, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_zip_codes(%ZipCodes{} = zip_codes, attrs) do
    zip_codes
    |> ZipCodes.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a zip_codes.

  ## Examples

      iex> delete_zip_codes(zip_codes)
      {:ok, %ZipCodes{}}

      iex> delete_zip_codes(zip_codes)
      {:error, %Ecto.Changeset{}}

  """
  def delete_zip_codes(%ZipCodes{} = zip_codes) do
    Repo.delete(zip_codes)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking zip_codes changes.

  ## Examples

      iex> change_zip_codes(zip_codes)
      %Ecto.Changeset{data: %ZipCodes{}}

  """
  def change_zip_codes(%ZipCodes{} = zip_codes, attrs \\ %{}) do
    ZipCodes.changeset(zip_codes, attrs)
  end
end
