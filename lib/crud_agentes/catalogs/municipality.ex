defmodule CrudAgentes.Catalogs.Municipality do
  use Ecto.Schema
  import Ecto.Changeset

  schema "cat_municipalities" do
    field :desciption, :string
    # field :id_state, :id,
    belongs_to :state, CrudAgentes.Catalogs.State,
      foreign_key: :id_state,
      references: :id
    has_one :zip_code, CrudAgentes.Catalogs.ZipCodes,
      foreign_key: :id_municipality,
      references: :id

    timestamps()
  end

  @doc false
  def changeset(municipality, attrs) do
    municipality
    |> cast(attrs, [:desciption])
    |> validate_required([:desciption])
  end
end
