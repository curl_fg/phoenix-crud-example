defmodule CrudAgentes.Catalogs.State do
  use Ecto.Schema
  import Ecto.Changeset
  alias CrudAgentes.Catalogs

  schema "cat_states" do
    field :desciption, :string
    has_many :municipalities, Catalogs.Municipality,
      foreign_key: :id_state,
      references: :id

    timestamps()
  end

  @doc false
  def changeset(state, attrs) do
    state
    |> cast(attrs, [:desciption])
    |> validate_required([:desciption])
  end
end
