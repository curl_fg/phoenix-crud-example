defmodule CrudAgentes.Catalogs.ZipCodes do
  use Ecto.Schema
  import Ecto.Changeset
  alias CrudAgentes.Catalogs

  schema "cat_zip_codes" do
    field :desciption, :string
    # field :id_municipality, :id
    belongs_to :municipality, Catalogs.Municipality,
      foreign_key: :id_municipality,
      references: :id

    timestamps()
  end

  @doc false
  def changeset(zip_codes, attrs) do
    zip_codes
    |> cast(attrs, [:desciption])
    |> validate_required([:desciption])
  end
end
