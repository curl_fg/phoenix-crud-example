defmodule CrudAgentes.Repo do
  use Ecto.Repo,
    otp_app: :crud_agentes,
    adapter: Ecto.Adapters.Postgres
end
