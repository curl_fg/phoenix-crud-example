defmodule CrudAgentesWeb.PageController do
  use CrudAgentesWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
