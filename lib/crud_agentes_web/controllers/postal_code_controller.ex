defmodule CrudAgentesWeb.PostalCodeController do
  use CrudAgentesWeb, :controller

  alias CrudAgentes.Catalogs

  def index(conn, %{"searching" => searching}) do
    match = Catalogs.get_municipality_state(searching)

    conn
    |> json(%{matches: match})
  end

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
