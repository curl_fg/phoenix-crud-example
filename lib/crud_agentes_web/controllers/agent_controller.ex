defmodule CrudAgentesWeb.AgentController do
  use CrudAgentesWeb, :controller

  alias CrudAgentes.Accounts
  alias CrudAgentes.Accounts.Agent

  def index(conn, %{"limit" => limit, "offset" => offset}) do
    {limit, _} = Integer.parse(limit)
    {offset, _} = Integer.parse(offset)

    agents =
      if limit == -1 or offset == -1 do
        Accounts.list_agents()
      else
        Accounts.list_agents(%{limit: limit, offset: offset})
      end

    if agents == [] do
      conn
      |> json(%{})
    else
      conn
      |> json(%{agents: agents})
    end
  end

  def index(conn, %{"searching" => searching}) do
    # changeset = Accounts.change_agent(%Agent{})
    agents = Accounts.search_agent(searching)

    if agents == [] or searching == "" do
      conn
      |> json(%{})
    else
      conn
      |> json(%{agents: agents})
    end
  end

  def index(conn, _params) do
    changeset = Accounts.change_agent(%Agent{})
    agents = Accounts.list_agents()

    render(conn, "index.html", agents: agents, changeset: changeset)
  end

  def get_count_agents(conn, _params) do
    no = Accounts.count_agents()

    conn
    |> json(%{no: no})
  end

  def new(conn, _params) do
    changeset = Accounts.change_agent(%Agent{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"agent" => agent_params}) do
    case Accounts.create_agent(agent_params) do
      {:ok, agent} ->
        conn
        |> put_flash(:info, "Agent created successfully.")
        |> redirect(to: Routes.agent_path(conn, :show, agent))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    agent = Accounts.get_agent!(id)
    render(conn, "show.html", agent: agent)
  end

  def edit(conn, %{"id" => id}) do
    agent = Accounts.get_agent!(id)
    changeset = Accounts.change_agent(agent)
    render(conn, "edit.html", agent: agent, changeset: changeset)
  end

  def update(conn, %{"id" => id, "agent" => agent_params}) do
    agent = Accounts.get_agent!(id)

    case Accounts.update_agent(agent, agent_params) do
      {:ok, agent} ->
        conn
        |> put_flash(:info, "Agent updated successfully.")
        |> redirect(to: Routes.agent_path(conn, :show, agent))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", agent: agent, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    agent = Accounts.get_agent!(id)
    {:ok, _agent} = Accounts.delete_agent(agent)

    conn
    |> json(%{:status => "Sucessfull"})

    # |> put_flash(:info, "Agent deleted successfully.")
    # |> redirect(to: Routes.agent_path(conn, :index))
  end
end
