defmodule CrudAgentesWeb.Router do
  use CrudAgentesWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CrudAgentesWeb do
    pipe_through :browser

    get "/", PageController, :index
    # resources "/agents", AgentController
    get "/agents_length", AgentController, :get_count_agents
    get "/agents", AgentController, :index
    get "/agents/:id/edit", AgentController, :edit
    get "/agents/new", AgentController, :new
    get "/agents/:id", AgentController, :show
    post "/agents", AgentController, :create
    patch "/agents/:id", AgentController, :update
    put "/agents/:id", AgentController, :update
    delete "/agents/:id", AgentController, :delete

    get "/postal_codes", PostalCodeController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", CrudAgentesWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: CrudAgentesWeb.Telemetry
    end
  end
end
