# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     CrudAgentes.Repo.insert!(%CrudAgentes.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias CrudAgentes.Repo
alias CrudAgentes.Accounts.Agent
alias CrudAgentes.Catalogs.State
alias CrudAgentes.Catalogs.Municipality
alias CrudAgentes.Catalogs.ZipCodes

Repo.insert!(%Agent{
  name: "Juan",
  p_surname: "Morales",
  m_surname: "Muñoz",
  age: 33,
  email: "jumomu@outlook.com",
  license: "1023868"
})

Repo.insert!(%Agent{
  name: "Uriel",
  p_surname: "Gonzalez",
  m_surname: "Feria",
  age: 22,
  email: "urgofe@outlook.com",
  license: "1023869"
})

Repo.insert!(%Agent{
  name: "Abril",
  p_surname: "Feria",
  m_surname: "Lopez",
  age: 21,
  email: "abfelo@hotlmail.com",
  license: "1023870"
})

Repo.insert!(%Agent{
  name: "Adrian",
  p_surname: "Rodriguez",
  m_surname: "Morales",
  age: 33,
  email: "adromo@gmail.com",
  license: "1023871"
})

Repo.insert!(%Agent{
  name: "Pablo",
  p_surname: "Hernandez",
  m_surname: "Feria",
  age: 40,
  email: "pahefe@outlook.com",
  license: "1023872"
})

Repo.insert!(%Agent{
  name: "Antonio",
  p_surname: "Quiroz",
  m_surname: "Rodriguez",
  age: 29,
  email: "anquro@outlook.com",
  license: "1023873"
})

Repo.insert!(%Agent{
  name: "Ismael",
  p_surname: "Mendez",
  m_surname: "Hernandez",
  age: 26,
  email: "ismehe@outlook.com",
  license: "1023874"
})

Repo.insert!(%Agent{
  name: "Cesar",
  p_surname: "Vazquez",
  m_surname: "Quiroz",
  age: 27,
  email: "cevaqu@hotlmail.com",
  license: "1023875"
})

Repo.insert!(%Agent{
  name: "David",
  p_surname: "Diaz",
  m_surname: "Mendez",
  age: 22,
  email: "dadime@hotlmail.com",
  license: "1023876"
})

Repo.insert!(%Agent{
  name: "Antonia",
  p_surname: "Ibarra",
  m_surname: "Vazquez",
  age: 21,
  email: "anibva@outlook.com",
  license: "1023877"
})

Repo.insert!(%Agent{
  name: "Julio",
  p_surname: "Muñoz",
  m_surname: "Diaz",
  age: 20,
  email: "jumudi@outlook.com",
  license: "1023878"
})

Repo.insert!(%Agent{
  name: "Mario",
  p_surname: "Feria",
  m_surname: "Ibarra",
  age: 39,
  email: "mafeib@gmail.com",
  license: "1023879"
})

Repo.insert!(%Agent{
  name: "Luigi",
  p_surname: "Lopez",
  m_surname: "Gonzalez",
  age: 38,
  email: "lulogo@outlook.com",
  license: "1023880"
})

Repo.insert!(%Agent{
  name: "Daisy",
  p_surname: "Lopez",
  m_surname: "Feria",
  age: 26,
  email: "dalofe@outlook.com",
  license: "1023881"
})

Repo.insert!(%Agent{
  name: "Wario",
  p_surname: "Morales",
  m_surname: "Rodriguez",
  age: 21,
  email: "wamoro@outlook.com",
  license: "1023882"
})

Repo.insert!(%Agent{
  name: "Francisco",
  p_surname: "Prieto",
  m_surname: "Peralta",
  age: 33,
  email: "frprpe@outlook.com",
  license: "1023883"
})

Repo.insert!(%Agent{
  name: "Manuel",
  p_surname: "Alvarez",
  m_surname: "Jacinto",
  age: 40,
  email: "maalja@hotlmail.com",
  license: "1023884"
})

Repo.insert!(%Agent{
  name: "Victoria",
  p_surname: "Loaeza",
  m_surname: "Diaz",
  age: 29,
  email: "vilodi@gmail.com",
  license: "1023885"
})

Repo.insert!(%Agent{
  name: "Abril",
  p_surname: "Morales",
  m_surname: "Rodriguez",
  age: 29,
  email: "abmoro@outlook.com",
  license: "1023886"
})

Repo.insert!(%Agent{
  name: "Adrian",
  p_surname: "Feria",
  m_surname: "Hernandez",
  age: 26,
  email: "adfehe@outlook.com",
  license: "1023887"
})

Repo.insert!(%Agent{
  name: "Pablo",
  p_surname: "Rodriguez",
  m_surname: "Quiroz",
  age: 27,
  email: "paroqu@outlook.com",
  license: "1023888"
})

Repo.insert!(%Agent{
  name: "Antonio",
  p_surname: "Hernandez",
  m_surname: "Mendez",
  age: 22,
  email: "anheme@outlook.com",
  license: "1023889"
})

Repo.insert!(%Agent{
  name: "Ismael",
  p_surname: "Quiroz",
  m_surname: "Vazquez",
  age: 21,
  email: "isquva@outlook.com",
  license: "1023890"
})

Repo.insert!(%Agent{
  name: "Cesar",
  p_surname: "Mendez",
  m_surname: "Diaz",
  age: 39,
  email: "cemedi@gmail.com",
  license: "1023891"
})

Repo.insert!(%Agent{
  name: "David",
  p_surname: "Vazquez",
  m_surname: "Ibarra",
  age: 38,
  email: "davaib@outlook.com",
  license: "1023892"
})

Repo.insert!(%Agent{
  name: "David",
  p_surname: "Hernandez",
  m_surname: "Muñoz",
  age: 26,
  email: "dahemu@outlook.com",
  license: "1023893"
})

Repo.insert!(%Agent{
  name: "Antonia",
  p_surname: "Quiroz",
  m_surname: "Feria",
  age: 21,
  email: "anqufe@hotlmail.com",
  license: "1023894"
})

Repo.insert!(%Agent{
  name: "Julio",
  p_surname: "Rodriguez",
  m_surname: "Lopez",
  age: 33,
  email: "jurolo@gmail.com",
  license: "1023895"
})

Repo.insert!(%Agent{
  name: "Mario",
  p_surname: "Hernandez",
  m_surname: "Lopez",
  age: 40,
  email: "mahelo@outlook.com",
  license: "1023896"
})

Repo.insert!(%Agent{
  name: "Luigi",
  p_surname: "Quiroz",
  m_surname: "Morales",
  age: 29,
  email: "luqumo@outlook.com",
  license: "1023897"
})

###############################################################################

Repo.insert!(%State{
  id: 1,
  desciption: "Aguascalientes"
})

Repo.insert!(%State{
  id: 2,
  desciption: "Chihuahua"
})

Repo.insert!(%State{
  id: 3,
  desciption: "Puebla"
})

Repo.insert!(%State{
  id: 4,
  desciption: "Oaxaca"
})

Repo.insert!(%State{
  id: 5,
  desciption: "CDMX"
})

# ---

Repo.insert!(%Municipality{
  id: 1,
  desciption: "Asientos",
  id_state: 1
})

Repo.insert!(%Municipality{
  id: 2,
  desciption: "Cosio",
  id_state: 1
})

Repo.insert!(%Municipality{
  id: 3,
  desciption: "El llano",
  id_state: 1
})

Repo.insert!(%Municipality{
  id: 4,
  desciption: "Tepezalá",
  id_state: 1
})

Repo.insert!(%Municipality{
  id: 5,
  desciption: "Ahumada",
  id_state: 2
})

Repo.insert!(%Municipality{
  id: 6,
  desciption: "Camargo",
  id_state: 2
})

Repo.insert!(%Municipality{
  id: 7,
  desciption: "Delicias",
  id_state: 2
})

Repo.insert!(%Municipality{
  id: 8,
  desciption: "El Tule",
  id_state: 2
})

Repo.insert!(%Municipality{
  id: 9,
  desciption: "Acajete",
  id_state: 3
})

Repo.insert!(%Municipality{
  id: 10,
  desciption: "Amozoc",
  id_state: 3
})

Repo.insert!(%Municipality{
  id: 11,
  desciption: "Caltepec",
  id_state: 3
})

Repo.insert!(%Municipality{
  id: 12,
  desciption: "Honey",
  id_state: 3
})

Repo.insert!(%Municipality{
  id: 13,
  desciption: "Abejones",
  id_state: 4
})

Repo.insert!(%Municipality{
  id: 14,
  desciption: "Salina Cruz",
  id_state: 4
})

Repo.insert!(%Municipality{
  id: 15,
  desciption: "Juchitán de Zaragoza",
  id_state: 4
})

Repo.insert!(%Municipality{
  id: 16,
  desciption: "La Pe",
  id_state: 4
})

Repo.insert!(%Municipality{
  id: 17,
  desciption: "Álvaro Obregón",
  id_state: 5
})

Repo.insert!(%Municipality{
  id: 18,
  desciption: "Iztapalapa",
  id_state: 5
})

Repo.insert!(%Municipality{
  id: 19,
  desciption: "Miguel Hidalgo",
  id_state: 5
})

# ---

Repo.insert!(%ZipCodes{
  id: 1,
  desciption: "20700",
  id_municipality: 1
})

Repo.insert!(%ZipCodes{
  id: 2,
  desciption: "20720",
  id_municipality: 1
})

Repo.insert!(%ZipCodes{
  id: 3,
  desciption: "20469",
  id_municipality: 2
})

Repo.insert!(%ZipCodes{
  id: 4,
  desciption: "20472",
  id_municipality: 2
})

Repo.insert!(%ZipCodes{
  id: 5,
  desciption: "20330",
  id_municipality: 3
})

Repo.insert!(%ZipCodes{
  id: 6,
  desciption: "20339",
  id_municipality: 3
})

Repo.insert!(%ZipCodes{
  id: 7,
  desciption: "20614",
  id_municipality: 4
})

Repo.insert!(%ZipCodes{
  id: 8,
  desciption: "20657",
  id_municipality: 4
})

Repo.insert!(%ZipCodes{
  id: 9,
  desciption: "32800",
  id_municipality: 5
})

Repo.insert!(%ZipCodes{
  id: 10,
  desciption: "32816",
  id_municipality: 5
})

Repo.insert!(%ZipCodes{
  id: 11,
  desciption: "33759",
  id_municipality: 6
})

Repo.insert!(%ZipCodes{
  id: 12,
  desciption: "33737",
  id_municipality: 6
})

Repo.insert!(%ZipCodes{
  id: 13,
  desciption: "33113",
  id_municipality: 7
})

Repo.insert!(%ZipCodes{
  id: 14,
  desciption: "33089",
  id_municipality: 7
})

Repo.insert!(%ZipCodes{
  id: 15,
  desciption: "33550",
  id_municipality: 8
})

Repo.insert!(%ZipCodes{
  id: 16,
  desciption: "33553",
  id_municipality: 8
})

Repo.insert!(%ZipCodes{
  id: 17,
  desciption: "75113",
  id_municipality: 9
})

Repo.insert!(%ZipCodes{
  id: 18,
  desciption: "75118",
  id_municipality: 9
})

Repo.insert!(%ZipCodes{
  id: 19,
  desciption: "72980",
  id_municipality: 10
})

Repo.insert!(%ZipCodes{
  id: 20,
  desciption: "72997",
  id_municipality: 10
})

Repo.insert!(%ZipCodes{
  id: 21,
  desciption: "75895",
  id_municipality: 11
})

Repo.insert!(%ZipCodes{
  id: 22,
  desciption: "75894",
  id_municipality: 11
})

Repo.insert!(%ZipCodes{
  id: 23,
  desciption: "73129",
  id_municipality: 12
})

Repo.insert!(%ZipCodes{
  id: 24,
  desciption: "73120",
  id_municipality:  12
})

Repo.insert!(%ZipCodes{
  id: 25,
  desciption: "68715",
  id_municipality: 13
})

Repo.insert!(%ZipCodes{
  id: 26,
  desciption: "70612",
  id_municipality: 14
})

Repo.insert!(%ZipCodes{
  id: 27,
  desciption: "70666",
  id_municipality: 14
})

Repo.insert!(%ZipCodes{
  id: 28,
  desciption: "70033",
  id_municipality: 15
})

Repo.insert!(%ZipCodes{
  id: 29,
  desciption: "70045",
  id_municipality: 15
})

Repo.insert!(%ZipCodes{
  id: 30,
  desciption: "71566",
  id_municipality: 16
})

Repo.insert!(%ZipCodes{
  id: 31,
  desciption: "71565",
  id_municipality:  16
})

Repo.insert!(%ZipCodes{
  id: 32,
  desciption: "01299",
  id_municipality: 17
})

Repo.insert!(%ZipCodes{
  id: 33,
  desciption: "09870",
  id_municipality: 18
})

Repo.insert!(%ZipCodes{
  id: 34,
  desciption: "11800",
  id_municipality: 19
})
