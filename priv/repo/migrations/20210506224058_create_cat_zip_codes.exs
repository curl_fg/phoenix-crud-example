defmodule CrudAgentes.Repo.Migrations.CreateCatZipCodes do
  use Ecto.Migration

  def change do
    create table(:cat_zip_codes) do
      add :desciption, :string
      add :id_municipality, references("cat_municipalities", on_delete: :nothing)

      timestamps()
    end

    create index(:cat_zip_codes, [:id_municipality])
  end
end
