defmodule CrudAgentes.Repo.Migrations.CreateAgents do
  use Ecto.Migration

  def change do
    create table(:agents) do
      add :name, :string
      add :age, :integer
      add :email, :string
      add :p_surname, :string
      add :m_surname, :string
      add :license, :string
      add :expire_license, :date

      timestamps()
    end
  end
end
