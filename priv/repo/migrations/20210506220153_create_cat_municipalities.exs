defmodule CrudAgentes.Repo.Migrations.CreateCatMunicipalities do
  use Ecto.Migration

  def change do
    create table(:cat_municipalities) do
      add :desciption, :string
      add :id_state, references("cat_states", on_delete: :nothing)

      timestamps()
    end

    create index(:cat_municipalities, [:id_state])
  end
end
