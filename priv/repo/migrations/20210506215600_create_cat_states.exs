defmodule CrudAgentes.Repo.Migrations.CreateCatStates do
  use Ecto.Migration

  def change do
    create table(:cat_states) do
      add :desciption, :string

      timestamps()
    end

  end
end
